# Packaging software with conda

This document contains a brief primer on packaging software for
distribution with [conda](https://conda.io).

## What is a recipe?

A recipe is a collection of files that define how to build a Conda
package. Each recipe minimally contains a `meta.yaml` file that
describes the package name and version, its dependencies, and how to
build it.

### `meta.yaml`

For a fairly exhaustive description of what you can put in the `meta.yaml` file, see
[Defining metadata (meta.yaml)](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html#meta-yaml).

### `build.sh`

You can either define the build script inline in the `meta.yaml` file
([example](https://github.com/conda-forge/gwpy-feedstock/blob/79c965de850fb69e621e0fecc5310ee50574d6fc/recipe/meta.yaml)),
otherwise `conda-build` will look for a `build.sh` script in the same
directory (`bld.bat` for Windows). The `build.sh` script is a shell
script that can do whatever you need it to do
([example](https://github.com/conda-forge/nds2-client-feedstock/blob/492436f4fa3c6b8a62dbc86dc68d79f818d251e3/recipe/build.sh)).

## Packaging something new for conda-forge

All new recipes should be submitted as a Pull Request to the
[conda-forge/staged-recipes](https://github.com/conda-forge/staged-recipes)
repository on GitHub. The basic instructions are (from
[here](https://conda-forge.org/#add_recipe)):

- create a new recipe in a sub-directory of the `/recipes` directory
  in `staged-recipes`,
- commit this new recipe to a branch of your fork, and propose a Pull
  Request
- fix the inevitable linting and CI issues

Recipes can look quite different between languages, here are some good
examples from IGWN Conda (feel free to suggest your own):

- Python:
  [gwpy](https://github.com/conda-forge/gwpy-feedstock/tree/79c965de850fb69e621e0fecc5310ee50574d6fc/recipe/),
- C/C++:
  [nds2-client](https://github.com/conda-forge/nds2-client-feedstock/tree/492436f4fa3c6b8a62dbc86dc68d79f818d251e3/recipe/),
- Multiple outputs:
  [lal](https://github.com/conda-forge/lal-feedstock/tree/13e6fd9ba32f15b25aaf68bc6ffe106398d171b1/recipe).

## Maintaining a conda-forge recipe on GitHub

Once a recipe has been reviewed and merged, a new feedstock repository
will be created to house the recipe, and all build and continuous
integration (CI) utilities required to automatically build and upload
packages. All changes to the recipe from that point should be made via
Pull Requests against the feedstock repository.

There are automatic bots that will post Pull Requests whenever they
think that the recipe is out of date. This is mainly done by searching
for tarballs in the same location as the current release tarball that
suggest a newer version has been uploaded.

## Building packages against Intel MKL

!!! warning
    This work is experimental, and has not been reviewed by anyone who knows
    what they are doing. We are seeking advice from the conda-forge team.

All of the IGWN Conda packages are built using the conda-forge build
infrastructure, which defaults to using `openblas` builds of BLAS,
affecting GSL, numpy, scipy, and similar libraries. It is possible to
configure a conda package to build against MKL-builds of BLAS, GSL, and
friends; supporting both `openblas` and `mkl` builds at the same time
(switching using command-line options) requires some thought, but isn't
too hard.

If you have a recipe that is configured correctly, you can select an MKL
build from the command line using something like this:

```shell
conda build recipe --variants "{blas_impl: \"mkl\", mkl: \"2018\"}"
```

## Testing package builds

### Basics

In principle, testing a package build should be as simple as installing
`conda-build` and then executing `conda build`:

```shell
cd /path/to/recipe/parent/dir
conda build recipe/
```

e.g:

```shell
git clone https://github.com/conda-forge/lal-feedstock.git
cd lal-feedstock
conda build recipe
```

## Common issues and workarounds

If your build has failed, try one of the following hacky workarounds. If
that doesn't work, try google.

### (linux) C compiler doesn't see the `USER` variable

Add `USER` to the `build/script-env` list of variables to be inherited
into `build.sh`:

```yaml
build:
  script_env:
    - USER
```

### (macOS) Build tries to use the wrong version of the macOS SDK

Make sure and read [_Installing the macOS SDK_](compiling.md#installing-the-macos-sdk).

Then you can tell `conda-build` about this by adding the following lines
to `~/conda_build_config.yaml` (create it if you don't have it):

```yaml
CONDA_BUILD_SYSROOT:     # [osx]
  - /opt/MacOSX10.9.sdk  # [osx]
```

## Docker containers

If you prefer to work in a pristine environment, you can use the
[condaforge/linux-anvil](https://hub.docker.com/r/condaforge/linux-anvil/)
docker container; this is what is used by conda-forge feedstock CIs when
building packages for production.

## Compiling packages in a Conda environment, not using conda-build

This section has been moved to [Compiling](compiling.md).
