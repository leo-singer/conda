#!/usr/bin/env python3

"""Render the IGWN Conda environments from the package lists
"""

import argparse
import atexit
import json
import logging
import os
import shutil
import subprocess
import sys
import tempfile
from pathlib import Path

import git
from git.exc import GitError

import jinja2

import yaml

from conda_build.metadata import (select_lines, yamlize)

import utils

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

# create a logger
LOGGER = utils.create_logger("render")

# git repo
REPO = git.Repo()
try:
    ACTIVE_BRANCH = REPO.active_branch.name
except TypeError:
    if "CI_COMMIT_REF_NAME" not in os.environ:
        raise
    ACTIVE_BRANCH = os.environ["CI_COMMIT_REF_NAME"]

REMOTE_BRANCHES = [
    x.name[len(x.remote_name)+1:] for x in REPO.remotes.origin.refs if
    x.is_valid() and not x.name.endswith('HEAD')
]

# default file paths
_BASE_PATH = Path(__file__).parent.parent.resolve()
_DEFAULT_PACKAGES_PATH = _BASE_PATH / "packages"
_CONFIG_YAML_NAME = "igwn_conda_config.yaml"
_CONFIG_YAML_PATH = _BASE_PATH / _CONFIG_YAML_NAME
_UPSTREAM_YAML_NAME = "upstream.yaml"
_UPSTREAM_YAML_PATH = _DEFAULT_PACKAGES_PATH / _UPSTREAM_YAML_NAME
try:
    DEFAULT_CONFIG_YAML = _CONFIG_YAML_PATH.resolve(strict=True)
except FileNotFoundError:
    DEFAULT_CONFIG_YAML = None
try:
    DEFAULT_UPSTREAM_YAML = _UPSTREAM_YAML_PATH.resolve(strict=True)
except FileNotFoundError:
    DEFAULT_UPSTREAM_YAML = None

# YAML files to ignore
IGNORE = {
    "template.yaml",
    _UPSTREAM_YAML_NAME,
}

# test file templates
TEST_TEMPLATE = {
    "imports": """
#!/usr/bin/env python{{ python_version }}
# -*- coding: utf-8 -*-

import importlib

import pytest

@pytest.mark.parametrize("module", [
{% for item in imports %}
    "{{ item }}",{% endfor %}
])
def test_import(module):
    importlib.import_module(module)
""".strip(),
    "commands": """
#!/usr/bin/env python{{ python_version }}
# -*- coding: utf-8 -*-

import os
import subprocess

import pytest

@pytest.mark.parametrize("command", [
{% for item in commands %}
    \"\"\"{{ item|replace('"', '\\\\"') }}\"\"\",{% endfor %}
])
def test_command(command):
    subprocess.check_call(command, shell=True, env=os.environ)
""".strip(),
}

# YAML jinja2 variables
DEFAULT_YAML_CONFIG = {
    "py": int("{0.major}{0.minor}".format(sys.version_info)),
    "linux": sys.platform.startswith("linux"),
    "osx": sys.platform == "darwin",
}


def _latest_tag(default_branch="master"):
    """Returns the name of the most recent tag
    """
    try:
        return REPO.git.describe(abbrev=0)
    except GitError:
        try:
            return os.environ["CI_COMMIT_TAG"]
        except KeyError:
            if default_branch in REMOTE_BRANCHES:
                return default_branch
            return ACTIVE_BRANCH


def _target_branch_ref(target):
    # target should be either 'master' (proposed) or 'testing'
    if os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME") == target:
        # if this is a MR against the target, just use the current ref
        return os.getenv("CI_COMMIT_REF_NAME")

    if target in REMOTE_BRANCHES:
        return target

    raise RuntimeError(
        "could not locate ref for {}, available branches: {}".format(
            target, ", ".join(map(str, REMOTE_BRANCHES)),
        ),
    )


def _git_checkout(ref):
    """Checkout a specific git reference
    """
    LOGGER.debug(f"$ git checkout {ref}")
    REPO.git.checkout(ref)
    if REPO.head.is_detached:
        active = REPO.git.describe()
    else:
        active = REPO.active_branch.name
    LOGGER.debug(f"successfully checked out {active}")


def parse_yaml(data, **config):
    """Read a YAML file from a path
    """
    namespace = DEFAULT_YAML_CONFIG.copy()
    namespace.update(config)
    # render the YAML file using jinja2 to resolve internal variable usage
    data = jinja2.Template(data).render(**namespace)
    # evaluate selectors and return
    return yamlize(select_lines(data, namespace, False))


def find_packages(*bases):
    """Find the YAML files for all packages
    """
    for base in bases:
        for pkgfile in base.rglob("*.yaml"):
            if pkgfile.name in IGNORE:
                continue
            yield pkgfile


def find_test_scripts(*bases):
    """Find test scripts inside the package dirs
    """
    for base in bases:
        for ext in {".py", ".sh"}:
            for script in base.rglob(f"*{ext}"):
                yield script


def define_environments(branch, config, upstream=None, unpin_upstream=False):
    # read upstream package list
    if upstream is not None:
        with open(upstream, "r") as fobj:
            upstream = fobj.read()

    if branch == ["testing"]:
        branches = ["testing", "unstable"]
    else:
        branches = [branch]

    environments = {}

    for pystr in config["python"]:
        pyver, pybuild = pystr.split()
        pyv = ".".join(pyver.split(".")[:2])
        py = int(pyv.replace(".", ""))
        envd = environments[pyv] = {}
        name = "{}py{}".format(config["prefix"], py)
        uplist = sorted(parse_yaml(upstream, py=py)["dependencies"])
        if args.unpin_upstream:
            uplist = [x.split('=')[0] for x in uplist]

        for branch in branches:
            if branch == "stable":
                ename = name
            else:
                ename = "{}-{}".format(name, branch)

            if branch == "unstable":
                deps = ["python={}".format(pyv)] + [
                    x.split('=', 1)[0] for x in uplist
                ]
            else:
                deps = ["python={}={}".format(pyver, pybuild)] + uplist

            envd[branch] = {
                "name": ename,
                "channels": config["channels"],
                "dependencies": deps,
                "tests": {"imports": [], "commands": []},
            }
    return environments


def parse_build_string(name, raw, py):
    try:
        return jinja2.Template(raw).render(py=py)
    except TypeError as exc:
        exc.args = (
            "failed to render build_string for {}: {}".format(
                name,
                str(exc),
            ),
        )
        raise


def add_package(envdict, name, version, build,
                test_imports=None, test_commands=None):
    for branch in envdict:
        if branch == "unstable":
            envdict[branch]["dependencies"].append(name)
        else:
            envdict[branch]["dependencies"].append(
                "{}={}={}".format(
                    name,
                    version,
                    build,
                ),
            )
        envdict[branch]["tests"]["imports"].extend(test_imports or [])
        envdict[branch]["tests"]["commands"].extend(test_commands or [])


def render_environment(env):
    prefix = tempfile.mktemp(prefix="render-{}".format(env["name"]))
    cmd = [
        "conda",
        "create",
        "--json",
        "--yes",
        "--dry-run",
        "--prefix", prefix,
    ] + env["dependencies"]
    LOGGER.debug("$ {}".format(" ".join(cmd)))
    proc = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    out, err = proc.communicate()
    out = json.loads(out)
    if proc.returncode:
        LOGGER.critical(out["error"])
        raise subprocess.CalledProcessError(
            proc.returncode,
            " ".join(cmd),
        )
    new = env.copy()
    new["dependencies"] = sorted([
        "{name}={version}={build_string}".format(**pkg) for
        pkg in out["actions"]["LINK"]
    ])
    return new


def render(name, ref, args):
    LOGGER.info("-- Rendering {} {}".format(name, "".ljust(30, "-")))

    # checkout git ref
    _git_checkout(ref)

    # parse configuration
    with open(args.config_file, "r") as fobj:
        config = parse_yaml(fobj.read())
    LOGGER.info("Parsed configuration")

    environments = define_environments(
        name,
        config,
        upstream=args.upstream_file,
        unpin_upstream=args.unpin_upstream,
    )
    LOGGER.info("The following environments will be created:")
    for pyv in environments:
        for type_ in environments[pyv]:
            LOGGER.info(environments[pyv][type_]["name"])

    # -- read package list and render environments ---

    for pkgf in find_packages(*package_dirs):
        LOGGER.debug(f"parsing {pkgf.name}")
        with open(pkgf, "r") as fobj:
            pkgdef = fobj.read()
        for pyv in environments:
            py = int("".join(pyv.split(".", 2)))

            # parse YAML definitions for this version of python
            try:
                pkg = parse_yaml(pkgdef, py=py)
            except Exception as exc:
                exc.args = (
                    "failed to parse {}: {}".format(pkgf, str(exc)),
                )
                raise

            # verify that we want this package for this version
            if pkg.get("skip", False):
                continue

            # get information
            pkgname = pkg["package"]["name"]
            version = str(pkg["package"]["version"])
            rawbuild = str(pkg["package"]["build_string"])
            test = pkg.get("test", {})
            test_imports = test.get("imports", [])
            test_commands = test.get("commands", [])

            if any(x.startswith(pkgname) for x in environments[pyv][name]):
                raise ValueError("{!r} specified in {} and {}".format(
                    pkgname,
                    pkgf,
                    args.upstream_file,
                ))

            # parse build string
            build = parse_build_string(pkgname, rawbuild, py)

            # store package for this environment set
            add_package(
                environments[pyv],
                pkgname, version, build,
                test_imports, test_commands,
            )

    # -- render environments and write tests --------

    envbase = Path(args.output_dir).resolve()
    envbase.mkdir(exist_ok=True, parents=True)

    for pyv in environments:
        for envtype, env in environments[pyv].items():
            name = env["name"]
            tests = env.pop("tests")
            envdir = envbase / name
            envdir.mkdir(exist_ok=True)

            LOGGER.info("-- Finalising {}".format(name))

            env["dependencies"].sort()

            # render environment
            if envtype == "unstable":
                rendered = env
            else:
                rendered = render_environment(env)

            # write environment file
            yamlpath = envdir / "{}.yaml".format(name)
            with yamlpath.open("w") as out:
                yaml.dump(rendered, out, default_flow_style=False)
            LOGGER.info("Environment file written: {}".format(yamlpath))

            # write tests
            for test, template in TEST_TEMPLATE.items():
                testpath = envdir / "{}-test-{}.py".format(name, test)
                with testpath.open("w") as out:
                    print(
                        jinja2.Template(template).render(
                            python_version=pyv,
                            **{test: tests[test]}
                        ),
                        file=out,
                    )
                LOGGER.info(f"{test} tests script written: {testpath}")

            # copy test scripts into environment
            for script in find_test_scripts(*package_dirs):
                shutil.copyfile(script, envdir / script.name)
                LOGGER.debug(f"copied {script.name!r} to {envdir!s}")

# -- define command line and parse --------------

parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "packages",
    metavar="DIR",
    nargs="*",
    default=[str(_BASE_PATH / "packages")],
    help="path to packages dir (can be given multiple times)",
)
parser.add_argument(
    "-m",
    "--config-file",
    metavar=_CONFIG_YAML_NAME,
    default=DEFAULT_CONFIG_YAML,
    required=DEFAULT_CONFIG_YAML is None,
    type=Path,
    help="path to %(metavar)s",
)
parser.add_argument(
    "-u",
    "--upstream-file",
    metavar=_UPSTREAM_YAML_NAME,
    default=DEFAULT_UPSTREAM_YAML,
    type=Path,
    help="path to upstream package list",
)
parser.add_argument(
    "--stable-ref",
    default=_latest_tag(),
    help="name of stable ref",
)
parser.add_argument(
    "--proposed-ref",
    default=_target_branch_ref("master"),
    help="name of proposed branch",
)
parser.add_argument(
    "--testing-ref",
    default=_target_branch_ref("testing"),
    help="name of testing branch",
)
parser.add_argument(
    "--skip-stable",
    action="store_true",
    default=False,
    help="skip rendering stable environments",
)
parser.add_argument(
    "--skip-proposed",
    action="store_true",
    default=False,
    help="skip rendering proposed environments",
)
parser.add_argument(
    "--skip-testing",
    action="store_true",
    default=False,
    help="skip rendering testing environments",
)
parser.add_argument(
    "-o",
    "--output-dir",
    metavar="ENVDIR",
    default=_BASE_PATH / "environments",
    help="output directory for environments",
)
parser.add_argument(
    "-U",
    "--unpin-upstream",
    action="store_true",
    default=False,
    help="remove version pins from upstream",
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    default=False,
    help="print verbose output",
)
args = parser.parse_args()

if args.verbose:
    LOGGER.setLevel(logging.DEBUG)

package_dirs = set(Path(d).resolve() for d in args.packages)

# -- process each ref ---------------------------

atexit.register(_git_checkout, ACTIVE_BRANCH)

for name, (ref, skip) in {
        "stable": (args.stable_ref, args.skip_stable),
        "proposed": (args.proposed_ref, args.skip_proposed),
        "testing": (args.testing_ref, args.skip_testing),
}.items():
    if skip:
        continue
    render(name, ref, args)
