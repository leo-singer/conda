---

# -- configuration ------------------------------------------------------------

stages:
  - render
  - test
  - build
  - docs
  - rsync
  - deploy

variables:
  APT_GET: "apt-get -y -q -o dir::cache::archives=${CI_PROJECT_DIR}/.cache/apt"
  CVMFS_REPO: "oasis.opensciencegrid.org"
  CVMFS_PATH: "/cvmfs/${CVMFS_REPO}/ligo/sw/conda"
  GIT_STRATEGY: "clone"
  OASIS_USER: "ouser.ligo"
  OASIS_HOST: "oasis-login.opensciencegrid.org"
  OASIS_PATH: "/home/login/${OASIS_USER}/ligo/deploy/sw/conda"
  PIP_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/pip"
  RSYNC_OPTS: "--archive --compress --recursive --rsh=ssh --verbose"

.retry: &define-retry |
  retry() {
    local n=1
    local max=3
    local delay=15
    while true; do
      "$@" && break || {
        if [[ $n -lt $max ]]; then
          ((n++))
          echo "Command failed. Attempt $n/$max:"
          sleep $delay;
        else
          echo "The command has failed after $n attempts." 1>&2
          exit 1
        fi
      }
    done
  }

.docker_tag: &define-docker-tag |
  if [ -z ${CI_COMMIT_TAG} ]; then
      DOCKER_TAG="${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}";
  else
      DOCKER_TAG="${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}";
  fi
  export DOCKER_TAG

.debian: &debian
  image: debian:buster
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/apt

.apt-init: &apt-init |
  mkdir -p "${CI_PROJECT_DIR}/.cache/apt/partial" &&
  ${APT_GET} update

.install-miniconda-osx: &install-miniconda-osx |
  curl -Lo ./miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
  /bin/bash miniconda.sh -b -u -p ${CONDA_PATH:=${CI_PROJECT_DIR}/miniconda}

.init-conda: &init-conda |
  source "${CONDA_PATH:-/opt/conda}/etc/profile.d/conda.sh"
  conda config --set always_yes yes
  conda config --prepend channels conda-forge
  conda config --append channels igwn
  conda update --name base --yes conda
  conda info --all

.configure_oasis_ssh: &configure-oasis-ssh |
  ${APT_GET} install coreutils openssh-client;
  eval $(ssh-agent -s);
  mkdir -p ~/.ssh;
  ssh-add <(echo "${OASIS_PRIVATE_KEY}" | base64 -d);
  echo -e "Host ${OASIS_HOST}\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config;

.conda-clean: &conda-clean |
  source "${CONDA_PATH:-/opt/conda}/etc/profile.d/conda.sh"
  conda clean --yes --tarballs

# -- basic job types

.linux: &linux
  image: igwn/base:conda
  before_script:
    - *define-retry
    - *init-conda
  after_script:
    - *conda-clean

.osx: &osx
  tags:
    - macos
  before_script:
    - export CONDA_PATH="${CI_PROJECT_DIR}/miniconda"
    - *define-retry
    - *install-miniconda-osx
    - *init-conda
  after_script:
    - export CONDA_PATH="${CI_PROJECT_DIR}/miniconda"
    - *conda-clean

# -----------------------------------------------------------------------------

# -- render -----------------

.render: &render
  stage: render
  variables:
    GIT_DEPTH: 0
    GIT_STRATEGY: "clone"
  script:
    - conda activate base
    - conda install --yes --file requirements.txt
    - python ./scripts/render.py
          --config-file ./igwn_conda_config.yaml
          --output-dir environments/${CI_JOB_NAME#render:}
          --verbose
          packages
  artifacts:
    paths:
      - environments/
    expire_in: 1 day
  only:
    - branches@computing/conda
    - tags@computing/conda
    - merge_requests

# --

render:linux:
  <<: *linux
  <<: *render

render:osx:
  <<: *osx
  <<: *render

# -- test -------------------

.test: &test
  stage: test
  script:
    # parse job name to get OS and environment
    - read OS_NAME ENV_NAME <<<$(echo ${CI_JOB_NAME} | awk -F':' '{print $2,$3}')
    - ENV_DIR="environments/${OS_NAME}/${ENV_NAME}"
    # create environment
    - retry conda env create --file ${ENV_DIR}/${ENV_NAME}.yaml
    - conda activate ${ENV_NAME}
    # print packages (helps debugging)
    - conda list
    # run tests
    - cd ${ENV_DIR}
    - python -m pytest
          ${ENV_NAME}-test-*.py
          --verbose
          --junitxml="${CI_PROJECT_DIR}/${ENV_NAME}-test-report.xml"
  artifacts:
    reports:
      junit: "*-test-report.xml"

.test-linux: &test-linux
  <<: *linux
  <<: *test
  needs:
    - render:linux

.test-osx: &test-osx
  <<: *osx
  <<: *test
  needs:
    - render:osx

.test-stable: &test-stable
  only:
    - tags@computing/conda

.test-proposed: &test-proposed
  only:
    - branches@computing/conda
    - tags@computing/conda
    - merge_requests
  except:
    - testing@computing/conda

.test-testing: &test-testing
  only:
    - branches@computing/conda
    - tags@computing/conda
    - merge_requests
  except:
    - master@computing/conda

# --

#test:linux:igwn-py36:
#  <<: *test-linux
#  <<: *test-stable
#
#test:linux:igwn-py36-proposed:
#  <<: *test-linux
#  <<: *test-proposed
#
test:linux:igwn-py36-testing:
  <<: *test-linux
  <<: *test-testing

test:linux:igwn-py37:
  <<: *test-linux
  <<: *test-stable

test:linux:igwn-py37-proposed:
  <<: *test-linux
  <<: *test-proposed

test:linux:igwn-py37-testing:
  <<: *test-linux
  <<: *test-testing

#test:osx:igwn-py36:
#  <<: *test-osx
#  <<: *test-stable
#
#test:osx:igwn-py36-proposed:
#  <<: *test-osx
#  <<: *test-proposed
#
test:osx:igwn-py36-testing:
  <<: *test-osx
  <<: *test-testing

test:osx:igwn-py37:
  <<: *test-osx
  <<: *test-stable

test:osx:igwn-py37-proposed:
  <<: *test-osx
  <<: *test-proposed

test:osx:igwn-py37-testing:
  <<: *test-osx
  <<: *test-testing

test:docker:
  stage: test
  image: docker:latest
  needs:
    - render:linux
  script:
    # build out the environments using docker build
    - docker build .
          --file "scripts/build-oasis.dockerfile"
          --pull
          --no-cache
          --build-arg "INSTALL_PATH=${CVMFS_PATH}"
          ${BUILD_OPTS}
  only:
    refs:
      - merge_requests
    changes:
      - scripts/build.py
      - scripts/build-oasis.dockerfile

# -- CVMFS build ------------

.build: &build
  stage: build
  image: docker:latest
  needs:
    - render:linux
  script:
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - *define-docker-tag
    # build out the environments using docker build
    - docker build .
          --file "scripts/build-oasis.dockerfile"
          --pull
          --no-cache
          --tag "${DOCKER_TAG}"
          --build-arg "INSTALL_PATH=${CVMFS_PATH}"
          --build-arg "BUILD_OPTS=${BUILD_OPTS}"
    - docker push "${DOCKER_TAG}"
  only:
    refs:
      - branches@computing/conda
      - tags@computing/conda

build:stable:
  <<: *build
  before_script:
    - BUILD_OPTS="--tag ${CI_COMMIT_TAG} --skip-proposed --skip-testing"
  only:
    - tags@computing/conda

build:proposed:
  <<: *build
  before_script:
    - BUILD_OPTS="--skip-testing"
  only:
    - master@computing/conda

build:testing:
  <<: *build
  before_script:
    - BUILD_OPTS="--skip-proposed"
  only:
    - testing@computing/conda

# -- CVMFS rsync ------------

.rsync_oasis: &rsync-oasis
  <<: *debian
  stage: rsync
  before_script:
    # install software
    - *apt-init
    - *configure-oasis-ssh
    - ${APT_GET} install
          docker.io
          gzip
          rsync
          tar
    # authenticate with local container registry
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - *define-docker-tag
    # run, then export the container into a tarbll
    - docker pull "${DOCKER_TAG}"
    - DOCKER_NAME="tmp${RANDOM}"
    - docker run -itd --name ${DOCKER_NAME} "${DOCKER_TAG}" /bin/python
    - docker export ${DOCKER_NAME} | gzip -c > igwn-conda.tar.gz
    - docker stop ${DOCKER_NAME}
    - docker rm ${DOCKER_NAME}
    - docker rmi "${DOCKER_TAG}"
    # unpack the tarball
    - mkdir -p _build
    - cd _build
    - tar -xf ../igwn-conda.tar.gz
    - cd -
    # sanity check things locally first
    - head -n 1 ./_build/condabin/conda
    - ./_build/bin/python ./_build/condabin/conda info --all
  only:
    refs:
      - branches@computing/conda
      - tags@computing/conda
    variables:
      - $CI_COMMIT_MESSAGE !~ /\[skip deploy\]/

rsync:stable:
  <<: *rsync-oasis
  script:
    # sync the base conda environment
    - rsync
          ${RSYNC_OPTS}
          --delete
          --exclude envs
          ./_build/
          ${OASIS_USER}@${OASIS_HOST}:${OASIS_PATH}/
    # sync the stable environments
    - rsync
          ${RSYNC_OPTS}
          ./_build/envs/
          ${OASIS_USER}@${OASIS_HOST}:${OASIS_PATH}/envs/
  only:
    refs:
      - tags@computing/conda
    variables:
      - $CI_COMMIT_MESSAGE !~ /\[skip deploy\]/

rsync:proposed:
  <<: *rsync-oasis
  script:
    - rsync
          ${RSYNC_OPTS}
          --delete
          ./_build/envs/*-proposed
          ${OASIS_USER}@${OASIS_HOST}:${OASIS_PATH}/envs/
  only:
    refs:
      - master@computing/conda
    variables:
      - $CI_COMMIT_MESSAGE !~ /\[skip deploy\]/

rsync:testing:
  <<: *rsync-oasis
  script:
    - rsync
          ${RSYNC_OPTS}
          --delete
          ./_build/envs/*-testing
          ${OASIS_USER}@${OASIS_HOST}:${OASIS_PATH}/envs/
  only:
    refs:
      - testing@computing/conda
    variables:
      - $CI_COMMIT_MESSAGE !~ /\[skip deploy\]/

deploy:osg-oasis-update:
  <<: *debian
  stage: deploy
  before_script:
    - *apt-init
    - *configure-oasis-ssh
  script:
    - ssh ${OASIS_USER}@${OASIS_HOST} osg-oasis-update
  only:
    refs:
      - tags@computing/conda
      - master@computing/conda
      - testing@computing/conda
    variables:
      - $CI_COMMIT_MESSAGE !~ /\[skip deploy\]/

# -- documentation ----------

docs:
  <<: *linux
  stage: docs
  needs:
    - render:linux
    - render:osx
  variables:
    PIP_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/pip"
  script:
    - conda install --yes --file requirements.txt
    - conda install --yes pip mkdocs
    - python -m pip install -r docs/requirements.txt
    # convert environments
    - python -u scripts/write_env_md.py
    # copy environment YAML files into output HTML paths
    - |
      for OS_NAME in linux osx; do
          _TARGET="docs/environments/${OS_NAME}/";
          mkdir -p ${_TARGET};
          cp -v environments/${OS_NAME}/*/*.yaml ${_TARGET};
      done
    # render docs
    - mkdocs build --strict --verbose --site-dir site
  artifacts:
    paths:
      - site
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .cache/pip
  only:
    - branches@computing/conda
    - tags@computing/conda
    - merge_requests

pages:
  stage: deploy
  variables:
    GIT_STRATEGY: "none"
  needs:
    - docs
  only:
    - branches@computing/conda
    - tags@computing/conda
  script:
    - mv site public
  artifacts:
    paths:
      - public
