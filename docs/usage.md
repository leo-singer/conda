# Using the IGWN Conda Distribution

This page describes how to use the the IGWN Conda Distribution, either
via CVMFS, or with a standalone Conda configuration.

!!! note "OS support"
    The IGWN Conda Distribution is fully supported on 64-bit Linux and
    macOS, support for other platforms is on a best-effort basis only.

## 1. Configure conda {: #configure }

### CVMFS (recommended) {: #cvmfs }

The IGWN Conda Distribution is principally built and distributed via the
OASIS CVMFS repository managed by the
[Open Science Grid](https://opensciencegrid.org) project.
This repository is configured on all IGWN Computing Centres, and all
other centres that are part of the Open Science Grid distributed computing
network.

To configure conda from OASIS, just `source` the `conda.sh` setup script
as follows:

```bash
source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
```

The you should be able to run `conda --help` and other commands to start
working with conda.

!!! tip "Add this to `.bashrc` (or similar)"
    The above `source` command is fairly harmless, only adding a single
    directory to your `PATH` environment variable and setting up a few
    `bash` aliases and functions, so can easily be added to your
    bash login script (typically `~/.bashrc`) to configure conda
    automatically when you log in to a machine.

### Manual {: #miniconda }

For systems that don't have OASIS configured, users will need to install
conda manually.
We recommend
[installing Miniconda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html),
however you can also
[install conda using your Linux package manager](https://www.anaconda.com/rpm-and-debian-repositories-for-miniconda/).

## 2a. Activate a pre-built environment {: #activate }

The IGWN Conda Distribution on OASIS provides a number of pre-built
environments for Linux users, see [_Environments_](environments/index.md)
for full details of what environments are available, or run

```
conda env list
```

for a complete list.
Once you have chosen, simply run


```
conda activate <env>
```

to activate the relevant environment.

!!! example "Example: activating the `igwn-py37` environment from OASIS"

    ```
    source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
    conda activate igwn-py37
    ```

## 2b. Create an environment using an IGWN Conda environment file {: #create }

For systems where the pre-built environments are not available, users can
download the environment defintion YAML files for any IGWN Conda
Distribution environment and use `conda env create` to create local environments
manually. See the **Download** and **Install** sections on the pages for each
environment for download links and example `conda env create` usage.

!!! example "Example: creating a copy of `igwn-py37`"

    ```
    curl -O https://computing.docs.ligo.org/conda/environments/linux/igwn-py37.yaml
    conda env create --file igwn-py37.yaml
    ```

## HTCondor workflows

The IGWN Conda Distributions from OASIS can be used easily in HTCondor
workflows; for best results, follow these simple rules in your
HTCondor submit files:

1. use the absolute path of any executables

2. set `transfer_executable = False`

!!! example "Options required to use IGWN Conda in an HTCondor workflow"

    ```
    executable = /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/python
    transfer_executable = False
    ```

!!! tip "Submitting to the Open Science Grid"
    Remember that if you are submitting to the IGWN Computing Grid (OSG), you
    will need other requirements (e.g. `IS_GLIDEIN`). For more information, see
    [_Submitting to the Open Science Grid_](https://computing.docs.ligo.org/guide/condor/#osg)
    on the IGWN Computing Guide.

!!! tip "No environment settings necessary"
    If you supply the full path of the `executable` in your HTCondor submit
    file, you should **not** need to `activate` any conda environments before
    submitting the job/workflow, or pass `getenv=True` to your submit file.
    If you need environment settings for other reasons, you should use the
    `environment` command in your submit file, see
    [here](https://htcondor.readthedocs.io/en/stable/users-manual/submitting-a-job.html#environment-variables) for more details.
