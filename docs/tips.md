# Tips and tricks

## Clone an environment

You can clone one of the pre-built environments (or any environment,
generally) with

```shell
conda create --name <target> --clone <source>
```

e.g.:

```shell
conda create --name myigwn-py37 --clone igwn-py37
```

## Preserve your prompt

By default, activating a conda environment will change your login
prompt. This behaviour can be disabled with

```shell
conda config --set changeps1 no
```

## Using LaTeX

The IGWN Conda Distribution does not provide (La)TeX. You should
install TeX using your system package manager, see below for details for
some common operating systems:

!!! warning "Should be good enough for matplotlib"

    The following examples should get you far enough to use TeX with the
    Matplotlib Python library. If you find that other packages are needed,
    please [open a Bug
    ticket](https://git.ligo.org/computing/conda/issues/new?issuable_template=Bug).

### Macports

```shell
port install \
    texlive \
    dvipng
```

### Debian/Ubuntu

```shell
apt-get install \
    dvipng \
    texlive-latex-base \
    texlive-latex-extra
```

### RHEL/Centos/Fedora

```shell
yum install \
    texlive-dvipng-bin \
    texlive-latex-bin-bin \
    texlive-type1cm \
    texlive-collection-fontsrecommended
```
