# Creating a new stable environment release

[![Version format](https://img.shields.io/badge/calver-YY0M0D.MICRO-22bfda.svg)](https://calver.org/)

This document describes how to create a new stable release of the
IGWN Conda Distribution.

All you need to do is run `release.sh` from the root of the repository:

```bash
bash release.sh
```

!!! example "What does that do?"
    The [`release.sh`](https://git.ligo.org/computing/conda/blob/master/scripts/release.sh) script does the following:

    1. generates a tag name based on today's date; if a tag already exists
       for today, a 'micro' version number is appended as a suffix, e.g:

           ```text
           20190101
           20190101.1
           20190102
           ```

    2. updates the `master` branch

    3. executes `git tag`

    4. executes `git push`

!!! warning "GPG signatures required"
    All git interactions require a GPG key signature.
