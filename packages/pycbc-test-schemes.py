#!/usr/bin/env python

"""Test the availability of a various processing schemes for PyCBC
"""

import sys

import pytest

from pycbc import scheme

SKIP = {
    "cuda",
}


@pytest.mark.parametrize("klass, name", scheme.scheme_prefix.items())
def test_pycbc_scheme(klass, name):
    if name in SKIP:
        pytest.skip("ignoring {!r} scheme".format(name))
    klass()


if __name__ == "__main__":
    sys.exit(pytest.main(args=[__file__] + sys.argv[1:]))
