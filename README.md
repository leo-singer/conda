# IGWN Conda Distribution

[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Build status](https://git.ligo.org/computing/conda/badges/master/pipeline.svg)](https://git.ligo.org/computing/conda/commits/master)

The IGWN Conda Distribution (IGWN-Conda) is a programme to manage and
distribute software and environments used by the International
Gravitational-Wave Observatory Network (IGWN) using the
[conda](https://conda.io) package manager, the
[conda-forge](https://conda-forge.org) community, and the
[CernVM File System (CVMFS)](https://cernvm.cern.ch/portal/filesystem).

## Quickstart

**Use a pre-built environment (linux only):**

!!! warning "CVMFS is read-only"

    The pre-built environments are **read only**. If you need a modifiable
    environment, please see
    [Clone an environment](https://computing.docs.ligo.org/conda/tips/#clone),
    or
    [Managing packages](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-pkgs.html).

1. [Install and configure CVMFS](https://wiki.ligo.org/Computing/CvmfsUser);
   if you are running on the IGWN Computing Grid, **you will not need to do this**.

2. Configure `conda`:

       ```shell
       source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
       ```

3. Activate an environment:

       ```shell
       conda activate igwn-py37
       ```

**Or install miniconda yourself:**

1. [Install Miniconda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html).

2. Add the conda-forge channel:

       ```shell
       conda config --add channels conda-forge
       ```

3. Download one of the environment YAML files, see
   [_Environments_](https://computing.docs.ligo.org/conda/environments/).

4. Install the environment locally:

       ```shell
       conda env create --file <environment>.yaml
       ```

!!! tip "Environments"
    For full details of what environments are included in the distribution, and
    their contents, see
    [_Environments_](https://computing.docs.ligo.org/conda/environments/).

!!! tip "More helpful hints"
    See [_Tips and tricks_](https://computing.docs.ligo.org/conda/tips/)
    for more useful hints to help you best utilise the IGWN Conda Distribution.

## Contributing

If you would like to improve the IGWN Conda distributions, please
consider one of the following actions:

  - [Report a problem](https://git.ligo.org/computing/conda/issues/new?issuable_template=Bug)
  - [Request a new/updated package](https://git.ligo.org/computing/conda/issues/new?issuable_template=Request)
