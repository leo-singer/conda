# Package change workflow

This page attempts to describe the change workflow in order to implement
change requests that have been detailed on an SCCB ticket.

## Determining version numbers and build strings

In order to add a new package, or upgrade an existing package, the new package
version and build string need to be determined.

The version number should be declared in the SCCB ticket, which can then be used
to determine the build string by using `conda search`:

```console
conda search <package>=<version>
```

!!! example "conda search output"
    This will print something like this:

    ```console
    $ conda search python-ligo-lw=1.6.0
    Loading channels: done
    # Name                       Version           Build  Channel
    python-ligo-lw                 1.6.0  py27h0b31af3_0  conda-forge
    python-ligo-lw                 1.6.0  py27h0b31af3_1  conda-forge
    python-ligo-lw                 1.6.0  py36h0b31af3_0  conda-forge
    python-ligo-lw                 1.6.0  py36h0b31af3_1  conda-forge
    python-ligo-lw                 1.6.0  py37h0b31af3_0  conda-forge
    python-ligo-lw                 1.6.0  py37h0b31af3_1  conda-forge
    python-ligo-lw                 1.6.0  py38h0b31af3_0  conda-forge
    python-ligo-lw                 1.6.0  py38h0b31af3_1  conda-forge
    ```

Under normal circumstances you want to choose the highest build number (the `_1` suffix in the above example).

!!! tip "Determining the build string for all platforms"
    If the package in question is compiled, then it will likely have a
    different build string on Linux compared to that on macOS.
    You can search for a specific platform using the `--subdir` option to
    `conda search`:

    === "Linux"
        ```bash
        conda search --subdir=linux-64 <package>=<version>
        ```

    === "macOS"
        ```bash
        conda search --subdir=osx-64 <package>=<version>
        ```

!!! tip "Abstracting the python version"
    If the build string is the same across all python versions, you can
    abstract the python version in the YAML file by using the `{{ py }}`
    jinja2 substitution variable, even if the build string is different
    on different platforms:

    ```yaml
    package:
      name: python-ligo-lw
      version: 1.6.0
      build_string: py{{ py }}h516909a_1  # [linux]
      build_string: py{{ py }}h0b31af3_1  # [osx]
    ```

    See _[Selectors](../change-control.md#selectors)_ for more information on
    the jinja2 variables.

Once you have the version number and the build string(s) you can either add
a new file for the new package, or modify an existing file to upgrade an
existing package.

## Getting packages into testing

To get packages in testing,
[open a merge request](https://git.ligo.org/duncanmmacleod/conda/merge_requests/new?merge_request%5Btarget_branch%5D=testing) against `testing`.
The CI will run to validate things.

Once the CI passes, **the MR can be merged immediately** in order to deploy the
new packages into the testing environments.

## Migrating packages from testing to proposed

To migrate packages into proposed,
[open a merge request](https://git.ligo.org/duncanmmacleod/conda/merge_requests/new?merge_request%5Btarget_branch%5D=master) against `master` that implements the same changes.

!!! tip "Use `git cherry-pick`"
    How you implement the same changes is up to you, but `cherry-pick`ing
    the commits from `testing` is probably easiest.

!!! tip "Apply the right labels"
    Please then apply the _sccb::pending_ label to indicate that the SCCB
    is still considering this request.
    Please also apply any other labels that seem appropriate.

!!! warning "Wait for SCCB approval"
    Once the CI passes, **wait until the SCCB approves the request**.

As soon as the SCCB approves the request, the MR should then be merged to
deploy the new packages into the proposed environments.

## Creating a new stable release

When deployment time arrives, a new stable distribution should be created,
see [_Tagging a release_](./release.md).
